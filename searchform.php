<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="row">

		<div class="medium-12 columns">

			<div class="input-group show-for-medium">
				<input type="search" class="input-group-field search-field" placeholder="<?php echo esc_attr_x( 'Search for festivals, artists, concerts', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointstheme' ) ?>" />

				<div class="input-group-button">
					<a href="#" class="search-submit button fgt-search-button events-search-button"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/icn-search.png'; ?>" />  <span class="search-button-txt">SEARCH</span></a>
				</div>
			</div>
		</div>

	</div>
</form>

