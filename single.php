<?php get_header(); ?>
			
<div id="content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="row">
		<div class="small-12 columns">
			<h1 class="fgt-page-heading"><?php the_title() ?></h1>
		</div>
	</div>
    
	<div id="inner-content" class="row">

		<main id="main" class="small-12 medium-12 large-8 columns" role="main">

			<div class="fgt-main-column">

				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<section class="entry-content" itemprop="articleBody">

						<div class="fgt-featured-img">
							<?php the_post_thumbnail('full'); ?>
						</div>

						<div class="social_share" data-featured-image-url="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>"></div>

						<?php the_content(); ?>
						
					</section> <!-- end article section -->

					<footer class="article-footer">
						<p class="tags"><?php the_tags('<img src="' . get_template_directory_uri() . '/assets/images/icn-tag.png" /> ', ', ', ''); ?></p>	</footer> <!-- end article footer -->

					<?php comments_template(); ?>

				</article> <!-- end article -->

				<?php if ( function_exists( 'echo_ald_crp' ) ) echo_ald_crp(); ?>
				
			</div>

			<!-- Related Posts -->
				
			<?php get_template_part('parts/related_posts'); ?>

		</main> <!-- end #main -->

		<?php

		//get_sidebar();
		?>
		<div class="small-12 medium-12 large-4 columns">

		<?php
		// -----------------
		// Categories
		// -----------------
		echo '<div class="hide-for-small-only">';
		echo do_shortcode( '[fgt_categories]' );
		echo '</div>'; ?>

		<!-----------------
		// Ad Space
		// --------------->
		<div class="small-12 columns text-center hide-for-small-only">
			<div class="row fgt-widget-wrapper">
				<div class="full-width">
					<!-- 300x250/600 pos=502 -->
					<div id='ad-slot-502'>
						<script type='text/javascript'>
							googletag.cmd.push(function() { googletag.display('ad-slot-502'); });
						</script>
					</div>
				</div>

			</div>
		</div>



		<?php
		// --------------------------
		// Popular Posts
		// --------------------------
		echo '<div class="hide-for-small-only">';
		require_once(get_template_directory().'/features/popular-posts/index.php');
		echo '</div>'; ?>



		<?php
		// --------------
		// Instagram
		// -------------- ?>

		<div class="hide-for-small-only">
			<div class="row fgt-widget-margin-top">
				<div class="small-12 columns">
					<h4 class="fgt-widget-heading">INSTAGRAM</h4>
				</div>
			</div>

			<?php
			echo do_shortcode('[instagram-feed num=9 cols=3]'); ?>
		</div>


		<?php
		// -----------------
		// Tag Cloud
		// -----------------
		echo do_shortcode( '[fgt_tagcloud]' ); ?>

		<?php
		// --------------------------
		// Email Subscribe
		// --------------------------
		require_once(get_template_directory().'/features/email-subscribe/index.php'); ?>

		</div>



	</div> <!-- end #inner-content -->

	<?php endwhile; else : ?>

		<?php get_template_part( 'parts/content', 'missing' ); ?>

	<?php endif; ?>

	<div class="row fgt-leaderboard">
        <div class="small-12 columns text-center">                    
            <div id="ad-slot-501" class="leaderBoard">
                <script type="text/javascript">
                    googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
                </script>
            </div>
        </div>
    </div>

</div> <!-- end #content -->

<?php get_footer(); ?>