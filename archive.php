<?php get_header(); ?>
	<div id="content">
		<div class='row'>
			<div class='small-12 columns'>
				<h2 class='fgt-page-heading'><?php single_cat_title( '', true ); ?></h2>
			</div>
		</div>
		<div id="inner-content" class="row">
		
		    <main id="main" class="small-12 medium-8 large-8 columns" role="main">
		
				<?php $i = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<?php

					$thumb_big = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					$thumb_big_url = $thumb_big['0'];

					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
					$thumb_url = $thumb['0'];
					$excerpt = get_the_excerpt();

					$thumbURL = get_the_post_thumbnail_url($post->ID, 'medium');

					if(strlen($excerpt) > 155)
						$excerpt = preg_replace('/\s+?(\S+)?$/', '', substr($excerpt, 0, 155));

					?>
					
					<?php if($i === 0 ): ?>
						
						<div class="small-12 columns">
							<div class="row fgt-news-box-main">

								<div class="medium-12 columns">
									<div class="fgt-news-img-inner">
										<a href="<?= get_permalink() ?>"><img src="<?= $thumb_big_url; ?>" /></a>
									</div>

									<a href="<?= get_permalink() ?>">
										<h2 class="fgt-news-heading"><?= $post->post_title; ?></h2>
									</a>

									<p class="fgt-news-text">
										<?php echo excerpt_trim(get_the_excerpt()); ?>
										<span class="fgt-news-read-more">
											<a href="<?= get_permalink() ?>" class="fgt-red">(Continue Reading)</a>
										</span>
									</p>

								</div>

							</div>
						</div>
	
					<?php else: ?>
						
						<div class="small-12 columns">
							<div class="row fgt-news-box">
								<a href="<?= get_permalink() ?>" class="fgt-news-image" style="background-image: url('<?php echo $thumbURL ?>')"></a>
								<div class="fgt-news-content">
									<h2><a href="<?= get_permalink() ?>"><?= $post->post_title; ?></a></h2>
									<p>
										<?php echo excerpt_trim(get_the_excerpt()); ?>
										<span class="fgt-news-read-more">
										<a href="<?= get_permalink() ?>" class="fgt-red">(Continue Reading)</a>
										</span>
									</p>
								</div>
							</div>
						</div>
	
					<?php endif; ?>

				<?php $i++; endwhile; wp_reset_postdata(); ?>
					
					<?php fgt_pagination($wp_query->max_num_pages); ?>

				<?php else : ?>

					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>
		
			</main> <!-- end #main -->

			<div class="small-12 large-4 medium-4 columns">
				<?php
				// -----------------
				// Categories
				// -----------------
				echo do_shortcode( '[fgt_categories]' ); ?>

				<?php
				// -----------------
				// Tag Cloud
				// -----------------
				echo do_shortcode( '[fgt_tagcloud]' ); ?>
			</div>
	    
	    </div> <!-- end #inner-content -->

		<div class="row fgt-leaderboard">
	        <div class="small-12 columns text-center">                    
	            <div id="ad-slot-501" class="leaderBoard">
	                <script type="text/javascript">
	                    googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
	                </script>
	            </div>
	        </div>
	    </div>
	    
	</div> <!-- end #content -->

<?php get_footer(); ?>