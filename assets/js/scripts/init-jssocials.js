jQuery(document).ready(function() {
    
    jQuery(".social_share").each(function() {
        jQuery(this).jsSocials({
            shares: [
                {
                    share: "facebook",
                    label: "Share"
                },
                "twitter",
                {
                    share: "pinterest",
                    media: jQuery(this).data('featured-image-url')
                },
                {
                    share: "email",
                    logo: "fa fa-envelope"
                }
            ],
            showLabel: false,
            showCount: false,
            shareIn: "popup"
        });
    });

});