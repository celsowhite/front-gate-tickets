import $ from 'jquery';
import jQuery from 'jquery';

export default class {

    constructor() {
        //this.urlRoot = window.location.protocol + window.location.host;
        this.urlPath = '/wp-content/themes/wp-joint/features/email-subscribe/subscribe.php';
    }

    validateEmailAddress(emailAddress) {
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        return re.test(emailAddress);
    }

}
