import EmailSubscribe from './emailSubscribe.js';

(function($) {

    //Used for page specific code
    let bodyHasClass = function(cssClass) {
        return $('body').hasClass(cssClass);
    };

    $(function() {

        // -----------------------
        // Home Page
        // -----------------------
        if(bodyHasClass('home')) {

            // ------------------------------------------------------------
            // HOME PAGE CAROUSEL
            // ------------------------------------------------------------

            //Show all slides
            $('.fgt-main-carousel').find('.orbit-slide').removeClass('hide');

            // Initialize Carousel
            // hack to avoid 0px height issue with Orbit carousel
            setTimeout(function() {
                new Foundation.Orbit($('.orbit'));
            }, 0);

        }

        // -------------------
        // Email subscription
        // -------------------

        let subscribeHandler = function($el) {

            let emailSubscribe = new EmailSubscribe();
            let emailAddress = $el.val();

            //Email is valid, subscribe to list
            if(emailSubscribe.validateEmailAddress(emailAddress)) {
                let url = wpUrls.theme + '/features/email-subscribe/subscribe.php';

                let addMember = $.ajax({ type: "POST", url: url, data: {"email": emailAddress}, dataType: 'json' });

                addMember.done(function(res) {

                    let modal = new Foundation.Reveal($('#email-subscribe-modal'));
                    modal.open();
                });

                addMember.fail(function(res) {
                    console.log('something went wrong');
                });

                //Invalid Email
            } else {
                let modal = new Foundation.Reveal($('#email-subscribe-invalid'));
                modal.open();
            }

        };
        
        $('.email-subscribe-form').on('submit', function(e) {
            e.preventDefault();
            let $el = $(this).find('.email-address');
            subscribeHandler($el);
        });

        $('.mobile-sign-up-button').on('click', function(e) {
            e.preventDefault();
            $('.mobile-sign-up-form').slideToggle();
        });

        // -------------------
        // Related Posts
        // -------------------

        //Show first two rows initially
        $('.fgt-related-row:lt(2)').css('display', 'flex');

        $('.fgt-related-posts .button').on('click', function(e) {

            e.preventDefault();

            $('.fgt-related-row').attr('style', 'display: -webkit-flex; display: flex');

            //$('.fgt-related-row').css('display', 'flex');

            $(this).hide();

        });


        // ----------------
        // Search Submit
        // ----------------
        $('.fgt-search-button').on('click', function(e) {
            $(this).parents('form').submit();

            e.preventDefault();
        });


        // --------------------------------------
        // Replace select field to allow styling
        // --------------------------------------

        $( '.search-form select' ).selectmenu();

    });

})(jQuery);

