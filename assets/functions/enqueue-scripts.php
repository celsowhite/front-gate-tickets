<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.min.js', array( 'jquery' ), '6.0', true );
    
    // jQuery UI custom download. Just used to style the select field for events search
    wp_enqueue_style(' jqueryui-css ', get_template_directory_uri() . '/vendor/jquery-ui-1.11.4.custom/jquery-ui.min.css');
    wp_enqueue_script(' jqueryui-js ', get_template_directory_uri() . '/vendor/jquery-ui-1.11.4.custom/jquery-ui.min.js');

    // JS Socials
    wp_enqueue_style(' jssocials-css ', get_template_directory_uri() . '/vendor/jssocials/jssocials.css');
    wp_enqueue_style(' jssocials-flat-css ', get_template_directory_uri() . '/vendor/jssocials/jssocials-theme-flat.css');
    wp_enqueue_script(' jssocials-js ', get_template_directory_uri() . '/vendor/jssocials/jssocials.min.js', '', '', true);

    // Font Awesome
    wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' );

  // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

    // ES6 Bundle
    wp_enqueue_script( 'es6-js', get_template_directory_uri() . '/assets/js/bundle.js' );

    // Localize main script for accessing Wordpress URLs in JS
    $js_variables = array(
      'site'          => get_option('siteurl'),
      'theme'         => get_template_directory_uri(),
      'ajax_url'      => admin_url('admin-ajax.php')
    );
    
    wp_localize_script('es6-js', 'wpUrls', $js_variables);

}
add_action('wp_enqueue_scripts', 'site_scripts', 999);