<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Joints
 */

get_header();



// ----------------------
// Main Carousel
// ----------------------
require_once(get_template_directory().'/features/main-carousel/index.php'); ?>

<div id="content">

	<div class="row">
		<div class="large-12 columns">
			<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

				<header class="article-header">
					<h1 class="entry-title single-title text-center" itemprop="headline">
						FESTIVAL NEWS
					</h1>

					<?php //get_template_part( 'parts/content', 'byline' ); ?>
				</header> <!-- end article header -->
			</article>
		</div>
	</div>

	<div id="inner-content" class="row">

		<main id="main" class="large-8 medium-8 small-12 columns" role="main">

			<?php
			// --------------------------
			// News List
			// --------------------------
			require_once(get_template_directory().'/features/news-list/index.php'); ?>
			
			<?php
			// --------------
			// Instagram - Mobile
			// -------------- ?>
			
			<div class="show-for-small-only">
				<div class="row fgt-widget-margin-top">
					<div class="small-12 columns">
						<h4 class="fgt-widget-heading">INSTAGRAM</h4>
					</div>
				</div>

				<?php
				echo do_shortcode('[instagram-feed num=9 cols=3 disablemobile="true"]'); ?>
			</div>

		</main> <!-- end #main -->

		<div class="large-4 medium-4 small-12 columns">

			<!--
            // Ad Space
            -->
            <!-- 300x250/600 pos=502 -->
			<div class="small-12 columns text-center hide-for-small-only">
				<div class="row fgt-widget-wrapper">
					<div class="full-width">						
						<div id='ad-slot-502'>
							<script type='text/javascript'>
								googletag.cmd.push(function() { googletag.display('ad-slot-502'); });
							</script>
						</div>
					</div>
				</div>
			</div>

			<?php
			// --------------------------
			// Popular Posts
			// --------------------------
			echo '<div class="hide-for-small-only">';
			require_once(get_template_directory().'/features/popular-posts/index.php');
			echo '</div>';

			?>

			<?php
			// --------------
			// Instagram - Desktop
			// -------------- ?>
			
			<div class="hide-for-small-only">
				<div class="row fgt-widget-margin-top">
					<div class="small-12 columns">
						<h4 class="fgt-widget-heading">INSTAGRAM</h4>
					</div>
				</div>

				<?php
				echo do_shortcode('[instagram-feed num=9 cols=3]'); ?>
			</div>

			<?php

			// --------------------------
			// Subscribe to email list
			// --------------------------
			echo '<div class="hide-for-small-only">';
			require_once(get_template_directory().'/features/email-subscribe/index.php');
			echo '</div>';

			?>

		</div>

		<?php //get_sidebar('festival_details'); ?>

		</div>

		<div class="row fgt-leaderboard">
			<div class="small-12 columns text-center">                    
				<div id="ad-slot-501" class="leaderBoard">
					<script type="text/javascript">
						googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
					</script>
				</div>
			</div>
		</div>

		<?php
		// --------------------------
		// Subscribe to email list
		// --------------------------
		require_once(get_template_directory().'/features/email-subscribe/email-subscribe-large.php');

		?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
