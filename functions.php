<?php

// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 
require_once(get_template_directory().'/assets/functions/menu-walkers.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');


// ----------------
// FGT Features
// ----------------
require_once(get_template_directory().'/features/festivals/index.php');

//Categories shortcode
require_once(get_template_directory().'/features/categories/index.php');

//Tag Cloud shortcode
require_once(get_template_directory().'/features/tag-cloud/index.php');

//Fix issue with tag cloud generated links
require_once(get_template_directory().'/features/tag-cloud/includeCustomPostTypesInTagArchives.php');

//Events custom post type
require_once(get_template_directory().'/features/events/post-type-events.php');

//CRON Job - takes events from api and inserts them as posts to events post type
require_once(get_template_directory().'/features/events-api-cron/index.php');

//Main Carousel Custom Post Type
require_once(get_template_directory().'/features/main-carousel/post-type-main-carousel.php');

//Relevanssi Enhancements
require_once(get_template_directory().'/features/search/relevanssi-enhancements.php');

//AJAX Load More Enhancements
require_once(get_template_directory().'/features/ajax-load-more/alm-enhancements.php');

//Settings page

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Front Gate Tickets Settings',
        'menu_title'	=> 'FGT Settings',
        'menu_slug' 	=> 'fgt-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}

/*================================= 
Custom Title/Excerpt Lengths
=================================*/

// Title

function title_trim($title) {
	$characters = 75;
	return mb_strimwidth($title, 0, $characters, '...');
}

// Excerpt

function excerpt_trim($excerpt) {
	$characters = 100;
	return mb_strimwidth($excerpt, 0, $characters, '...');
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );