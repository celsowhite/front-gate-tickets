<?php 
/*
Template Name: Upcoming Festivals
*/

get_header();
?>

<div id="content">

	<div id="inner-content" class="row">

		<main id="main" class="large-12 medium-12 columns first" role="main">

			<h2 class='fgt-page-heading'><?php the_title(); ?></h2>

			<div class="hide-for-small-only events-search-filter"><?= get_search_form(false); ?></div>

			<?php while(have_posts()): the_post(); ?>

				<div class="fgt-events">

                    <?php
                        // Upcoming festivals alm query. The query args are set via our custom alm filter hook in features/ajax-load-more/alm-enhancements.php
                        echo do_shortcode('[ajax_load_more 
                                            id="upcoming_festivals" 
                                            transition_container_classes="search-grid" 
                                            posts_per_page="18" 
                                            scroll="true"
                                            button_label="More Festivals" 
                                            button_loading_label="Loading Festivals"]');
                    ?>

				</div>

				<div class="row">
					<div class="small-12 columns">
						<img class="fgt-divider-margin" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-div-lg.png'; ?>" />
					</div>
				</div>

				<?php fgt_pagination($festival_loop->max_num_pages); ?>

			<?php endwhile; ?>
				
		</main> <!-- end #main -->

		<?php //get_sidebar(); ?>

	</div> <!-- end #inner-content -->

    <div class="row fgt-leaderboard">
        <div class="small-12 columns text-center">                    
            <div id="ad-slot-501" class="leaderBoard">
                <script type="text/javascript">
                    googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
                </script>
            </div>
        </div>
    </div>

</div> <!-- end #content -->

<?php get_footer(); ?>