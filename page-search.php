<?php
/**
 * Template Name: Search
 *
 * @package WordPress
 * @subpackage Joints
 */

get_header(); ?>

<div id="content">

	<div class="row">
		<div class="large-12 columns">
			<?php

			// ------------------------
			// Search Results
			// ------------------------

			$page = 1;
			$page_limit = 12;

			require_once(get_template_directory().'/vendor/unirest/src/Unirest.php');
			require_once(get_template_directory().'/services/EventsAPIHelper.php');
			require_once(get_template_directory().'/services/Paginate.php');

			use FGT\Paginate;


			$per_page = 12; //Items to show per page
			$page_num = isset($_GET['page_no']) ? (int) $_GET['page_no'] : 1; //Current page number

			$paginate = new Paginate($per_page, $page_num);



			$api_token = isset($_SESSION['fgt_api_token']) ? $_SESSION['fgt_api_token'] : null;
			$api = new EventsAPIHelper();

			$events_data = $api->get_events($api_token, $limit);
			$status = $events_data->code;

			//If token is expired, get another token
			if($status == '401') {
				//echo 'getting new token';
				$_SESSION['fgt_api_token'] = $api->get_token();
				$api_token = isset($_SESSION['fgt_api_token']) ? $_SESSION['fgt_api_token'] : null;
				$events_data = $api->get_events($api_token);
			}

			$events = $events_data->body->data;

			$paginated_links_html = $paginate->createPageLinks($events);
			$events_paginated = $paginate->paginateArray($events);

			?>

			<!-- Heading -->
			<div class="fgt-events">
				<div class='row'>
					<div class='small-12 columns'>
						<h2 class='fgt-page-heading'>EVENTS</h2>
					</div>
				</div>

				<!-- Filter/Search Options -->
				<div class="row">
					<div class="medium-8 columns">

						<div class="input-group">
							<input type="text" placeholder="Search festivals, artists or venues" />
							<div class="input-group-button">
								<input type="submit" class="button" value="SEARCH">
							</div>
						</div>

					</div>
				</div>

				<div class='row'>

					<?php

					$event_template = '';
					$events_template = '';

					foreach($events_paginated as $event) {

						if($event->active): ?>

							<?php
							$event_time = date('F j', strtotime($event->doorsStart)); ?>

							<!-- Event Item Block -->
							<div class='event-block medium-4 small-12 columns'>
								<a href="<?= $event->url ?>" target="_blank">
									<img src='<?= $event->imageLargeUrl ?>' /><br />
									<div class='event-info-block'>
										<h4><?= $event->name ?></h4>
										<span class='event-time'><?= $event_time ?></span>
										<div class='event-location'>
											<span><?= $event->venue->name ?></span>
											<span class='float-right'><?= $event->city ?>, <?= $event->state ?></span>
										</div>
									</div>
								</a>
							</div>

						<?php endif; ?>

					<?php } ?>

				</div>

				<div class="row">
					<div class="small-12 columns text-center">
						<?= $paginated_links_html ?>
					</div>
				</div>


			</div>
		</div>
	</div>

</div> <!-- end #content -->

<?php get_footer(); ?>

<!--


-->
