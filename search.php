<?php get_header();

require_once(get_template_directory().'/services/EventsHelper.php');
$search_term = strtoupper(esc_attr(get_search_query()));
$total_count = $wp_query->found_posts;
$display_count = $wp_query->post_count;

?>

<div id="content">

	<div id="inner-content" class="row">

		<main id="main" class="large-12 medium-12 columns first" role="main">

			<h2 class='fgt-page-heading'><?php echo (isset($_GET['pagetitle'])) ? $_GET['pagetitle'] : "SEARCH" ?></h2>

			<div class="hide-for-small-only events-search-filter"><?= get_search_form(false); ?></div>

			<header class="search_header">
				<h4>
					<?php if(!empty($search_term) && ($total_count>0)): ?>
						<?= "$total_count" ?> <?php _e('results for ', 'jointstheme'); ?> <?php echo "\"$search_term\""; ?>
					<?php elseif(empty($search_term)) : ?> 
					<?php else : ?> 
						No Search Results
					<?php endif; ?>
				</h4>
				<a href="<?php echo get_permalink( get_page_by_path('upcoming-festivals')); ?>">View All Upcoming Festivals</a>
			</header>

			<?php if(have_posts()): ?>

				<div class="fgt-events">
											
					<?php 
					// Save all of the results post types.
					// Will use this to conditionally group festival and post content.
					$search_result_post_types = [];
					while (have_posts()) : the_post(); 
					array_push($search_result_post_types, get_post_type());
					endwhile; 
					?>
					
					<?php 
					// If there is a festival in the results for this page of search then loop through and render all festivals
					// under the same heading.
					if(in_array('festivals', $search_result_post_types)): ?>
						<h2>FESTIVALS</h2>
						<div class="search-grid">
							<?php while (have_posts()) : the_post(); ?>
								<?php if(get_post_type() == 'festivals'): ?>
									<?php get_template_part( 'parts/loop', 'search-results' ); ?>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>

					<?php 
					// If there is a post in the results for this page of search then loop through and render all posts
					// under the same heading.
					if(in_array('post', $search_result_post_types)): ?>
						<h2>FESTIVAL NEWS</h2>
						<div class="search-grid">
							<?php while (have_posts()) : the_post(); ?>
								<?php if(get_post_type() == 'post'): ?>
									<?php get_template_part( 'parts/loop', 'search-results' ); ?>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>

				</div>

				<div class="row">
					<div class="small-12 columns">
						<img class="fgt-divider-margin" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-div-lg.png'; ?>" />
					</div>
				</div>

				<?php joints_page_navi(); ?>

			<?php else: ?>
			
			<?php endif; wp_reset_query(); ?>
				
		</main> <!-- end #main -->

		<?php //get_sidebar(); ?>

	</div> <!-- end #inner-content -->

	<div class="row fgt-leaderboard">
		<div class="small-12 columns text-center">                    
			<div id="ad-slot-501" class="leaderBoard">
				<script type="text/javascript">
					googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
				</script>
			</div>
		</div>
	</div>

</div> <!-- end #content -->

<?php get_footer(); ?>

