<?php

//Get custom menus for the footer links
//This backup query is necessary to display
//the custom menus on the archive pages
//http://stackoverflow.com/questions/19321866/wp-nav-menu-returns-empty-when-query-string-contains-certain-parameters

$backup = $wp_query;
$wp_query = NULL;
$wp_query = new WP_Query(array('post_type' => 'post'));

$fgt_info_menu = wp_get_nav_menu_items('FGT Information');
$ticketing_menu = wp_get_nav_menu_items( 'Ticketing' );

$wp_query = $backup; ?>

<footer class="footer" role="contentinfo">

    <div id="inner-footer" class="row small-only-text-center">

        <div class="small-12 columns">
            <div class="footer-divider"></div>
        </div>

        <div class="large-5 medium-5 small-12 columns footer-copyright">

             <div class="row">

                <div class="small-12 medium-12 columns">

                    <div class="hide-for-small-only lg-copyright">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-fgt-nav.png'; ?>" alt="" />
                        <span>&copy; <?php echo date('Y'); ?> Front Gate Ticketing Solutions, LLC</span>
                        <span>All rights reserved.</span><!-- #878787 -->
                    </div>

                </div>

                <div class="small-12 medium-10 columns">

                    <div class="show-for-small-only">

                        <div class="mobile-footer-brand">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-fgt-nav.png'; ?>" alt="" />
                            <!--<h3 style="display:inline-block;">Front Gate Tickets</h3>-->
                        </div>

                        <!--<h3 class="hide-for-small-only">Front Gate Tickets</h3>-->
                        <span>&copy; <?php echo date('Y'); ?> Front Gate Ticketing Solutions, LLC</span>
                        <span>All rights reserved.</span><!-- #878787 -->

                    </div>

                </div>

            </div>
        </div>

        <div class="large-2 medium-2 small-12 columns">

            <h5>FGT INFORMATION</h5>

            <?php foreach($fgt_info_menu as $item): ?>

                <ul class="fgt-menu-list">
                    <li>
                        <a href="<?= $item->url ?>">
                            <?= $item->title ?>
                        </a>
                    </li>
                </ul>

            <?php endforeach; ?>
        </div>
        <div class="large-2 medium-2 small-12 columns">

            <h5>TICKETING</h5>

            <?php foreach($ticketing_menu as $item): ?>

                <ul class="fgt-menu-list">
                    <li>
                        <a href="<?= $item->url ?>">
                            <?= $item->title ?>
                        </a>
                    </li>
                </ul>

            <?php endforeach; ?>
        </div>

        <div class="large-3 medium-3 small-12 columns">
            <?php

            //---------------
            // Social Media
            //---------------
            require_once( get_template_directory() . '/features/festivals-social-media/SocialMedia.php' );
            $img_dir = get_stylesheet_directory_uri() . '/assets/images/social-media/';

            //Wrap in backup query to allow this to display on archive pages
            $backup = $wp_query;
            $wp_query = NULL;
            $wp_query = new WP_Query(array('post_type' => 'post'));

            $social = new SocialMedia();
            $social_obj = $social->get_sites(true);

            $wp_query = $backup; ?>

            <div class="fgt-social-media fgt-widget-margin-bottom">


                <div class="row collapse ">
                    <div class="small-12 columns text-center">
                        <?php if($social_obj !== NULL): ?>
                            <?php if(property_exists($social_obj, 'facebook') ): ?>

                                <div style="display:inline-block; text-align: center;">
                                    <a href="<?php echo $social_obj->facebook['link'] ?>">
                                        <img src="<?php echo $img_dir ?>facebook.png" class="fgt-social-grid-img-margin" />
                                    </a>
                                </div>

                            <?php endif; ?>

                            <?php if(property_exists($social_obj, 'twitter') ): ?>
                                <div style="display:inline-block; text-align: center;">
                                    <a href="<?php echo $social_obj->twitter['link'] ?>">
                                        <img src="<?php echo $img_dir ?>twitter.png" class="fgt-social-grid-img-margin" />
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php if(property_exists($social_obj, 'youtube') ): ?>
                                <div style="display:inline-block; text-align: center;">
                                    <a href="<?php echo $social_obj->youtube['link'] ?>">
                                        <img src="<?php echo $img_dir ?>youtube-2.png" class="fgt-social-grid-img-margin" />
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php if(property_exists($social_obj, 'instagram') ): ?>
                                <div style="display:inline-block; text-align: center;">
                                    <a href="<?php echo $social_obj->instagram['link'] ?>">
                                        <img src="<?php echo $img_dir ?>instagram.png" class="fgt-social-grid-img-margin" />
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php if(property_exists($social_obj, 'tumblr') ): ?>
                                <div style="display:inline-block; text-align: center;">
                                    <a href="<?php echo $social_obj->tumblr['link'] ?>">
                                        <img src="<?php echo $img_dir ?>tumblr.png" class="fgt-social-grid-img-margin" />
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div>

                </div>

            </div>

        </div>
    </div> <!-- end #inner-footer -->
</footer> <!-- end .footer -->

</div> <!-- end .off-canvas-wrapper-inner -->
</div> <!-- end .off-canvas-wrapper -->

<?php wp_footer(); ?>

<!-- Email Subscribe Thank You Modal -->
<div class="small reveal" id="email-subscribe-modal">
    <h1>Thank you!</h1>
    <p class="lead">You have successfully subscribed to our email list.</p>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>


<!-- Mobile Email Subscribe Form --
<div class="small reveal" id="email-subscribe-sign-up-modal" data-reveal>

    <h1>Email List Sign Up!</h1>

    <input class="email-address" type="text" placeholder="SUBSCRIBE TO THE FRONT GATE NEWSLETTER">
    <input type="submit" class="button mobile-sign-up-button" value="SIGN UP">

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

</div>
-->

<!-- Email Subscribe Thank You Modal -->
<div class="small reveal" id="email-subscribe-invalid" data-reveal>
    <h1>Invalid Email!</h1>
    <p class="lead">You must enter a valid email address.</p>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<!-- Mobile Adhesion pos=690 -->
<div class="row">
    <div class="small-12 columns text-center">
        <div id="ad-slot-690" class="adhesion closeableAd">
            <script type="text/javascript">
                googletag.cmd.push(function() { googletag.display('ad-slot-690'); });
            </script>
        </div>
    </div>
</div>


</body>
</html> <!-- end page -->