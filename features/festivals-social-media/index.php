<?php

    require_once(get_template_directory().'/features/festivals-social-media/SocialMedia.php');
    $img_dir = get_stylesheet_directory_uri() . '/features/festivals-social-media/images/';

    $social = new SocialMedia();
    $social_obj = $social->get_sites();

?>

<!--
// -------------------------
// Follow Festival Markup
// -------------------------
-->

<?php if($social_obj): ?>
<div class="fgt-social-media fgt-widget-margin-top fgt-widget-margin-bottom">
    <div class="row">
        <div class="small-12 columns">
            <h4 class="fgt-widget-heading">FOLLOW THE FEST!</h4>
        </div>

    </div>


    <div class="row collapse ">
        <div class="small-12 columns text-center">
            <?php if($social_obj !== NULL): ?>
            <?php if(property_exists($social_obj, 'facebook') ): ?>

                <div style="display:inline-block; text-align: center;">
                    <a href="<?php echo $social_obj->facebook['link'] ?>">
                        <img src="<?php echo $img_dir ?>facebook.png" class="fgt-social-grid-img-margin" />
                    </a>
                </div>

            <?php endif; ?>

            <?php if(property_exists($social_obj, 'twitter') ): ?>
                <div style="display:inline-block; text-align: center;">
                    <a href="<?php echo $social_obj->twitter['link'] ?>">
                        <img src="<?php echo $img_dir ?>twitter.png" class="fgt-social-grid-img-margin" />
                    </a>
                </div>
            <?php endif; ?>

            <?php if(property_exists($social_obj, 'youtube') ): ?>
                <div style="display:inline-block; text-align: center;">
                    <a href="<?php echo $social_obj->youtube['link'] ?>">
                        <img src="<?php echo $img_dir ?>youtube-2.png" class="fgt-social-grid-img-margin" />
                    </a>
                </div>
            <?php endif; ?>

            <?php if(property_exists($social_obj, 'instagram') ): ?>
                <div style="display:inline-block; text-align: center;">
                    <a href="<?php echo $social_obj->instagram['link'] ?>">
                        <img src="<?php echo $img_dir ?>instagram.png" class="fgt-social-grid-img-margin" />
                    </a>
                </div>
            <?php endif; ?>

            <?php if(property_exists($social_obj, 'tumblr') ): ?>
                <div style="display:inline-block; text-align: center;">
                    <a href="<?php echo $social_obj->tumblr['link'] ?>">
                        <img src="<?php echo $img_dir ?>tumblr.png" class="fgt-social-grid-img-margin" />
                    </a>
                </div>
            <?php endif; ?>
            <?php endif; ?>

        </div>

    </div>




</div>
<?php endif; ?>