<?php

class SocialMedia
{

    public function get_sites($options_page = false) {

        //posts take an id, but option pages need the string 'option'
        $identifier = ($options_page) ? 'option' : get_the_ID();

        if( have_rows('social_media', $identifier) ):
            $social_obj = new stdClass();

            // loop through the rows of data
            while ( have_rows('social_media', $identifier) ) : the_row();

                // display a sub field value
                $site = strtolower(get_sub_field('site'));

                //Using site name as object properties is a little odd, but saves from having to iterate
                //over an array and check the values again when rendering the template below.
                $social_obj->{$site} = ['title' => get_sub_field('site'), 'link' => get_sub_field('link')];

            endwhile;

            return $social_obj;

        else :

            // No rows found
            return null;

        endif;
    }

}