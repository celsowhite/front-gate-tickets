<?php
require_once(get_template_directory().'/services/UrlHelper.php');
use FGT\UrlHelper;

function fgt_categories_shortcode($atts, $content=null){

    $args = array(
        'type'                     => 'post',
        'orderby'                  => 'name',
        'order'                    => 'ASC',
        'taxonomy'                 => 'category',
        'pad_counts'               => false
    );

    $categories = get_categories( $args );
    
    $site_url = get_site_url();
    $url_config = ['include_parameters' => false ];
    $current_url = UrlHelper::getURL($url_config);

    $css_class = '';
    if ( !is_front_page() && is_home() ) {
        $css_class = 'fgt-active';
    }

    $count_posts = wp_count_posts();
    $published_posts_count = $count_posts->publish;


    $return = <<<EOT

    <div class="fgt-categories fgt-widget-margin-bottom">

        <div class="row">
            <div class="small-12 columns">
                <h4 class="fgt-widget-heading">CATEGORIES</h4>
            </div>
        </div>

        <a href='$site_url/festival-news'>

                <div class='row'>
                    <div class='small-12 columns' style='text-transform: uppercase;'>
                        <div class='category-item $css_class'>
                            ALL NEWS
                            <span class='float-right fgt-red'>( $published_posts_count )</span>
                        </div>
                    </div>
                </div>

            </a>
EOT;

        foreach($categories as $category) {

            //Determine if category should be active
            $cat_url = get_category_link($category->cat_ID);
            $is_active = ($cat_url === $current_url);

            //Assign active class to variable
            $css_class = ($is_active) ? 'fgt-active' : '';



            $return .= <<<EOT

            <a href='$cat_url'>

                <div class='row'>
                    <div class='small-12 columns' style='text-transform: uppercase;'>
                        <div class='category-item $css_class'>
                            $category->name
                            <span class='float-right fgt-red'>( $category->count )</span>
                        </div>
                    </div>
                </div>

            </a>
EOT;

        }

    $return .= '</div>';

    return $return;

}
add_shortcode('fgt_categories', 'fgt_categories_shortcode');