<?php

require(dirname(__FILE__) . '/../../../../../wp-load.php');

// ------------------------------------------
// Uncomment below line to activate cron job
// ------------------------------------------
add_action( 'fgt_poll_events', 'create_posts_from_api' );

function create_posts_from_api() {

    require_once(get_template_directory().'/vendor/unirest/src/Unirest.php');
    require_once(get_template_directory().'/services/EventsAPIHelper.php');
    require_once(get_template_directory().'/services/LogService.php');

    //Load wp core
    require(dirname(__FILE__) . '/../../../../../wp-load.php');

    $file = dirname(__FILE__) . '/events-cron-log.txt';

    $log = new \FGT\LogService($file);

    //$limit = 30;

    //$api_token = isset($_SESSION['fgt_api_token']) ? $_SESSION['fgt_api_token'] : null;
    $api = new EventsAPIHelper();
    $api_token = $api->get_token();

    $events_data = $api->get_events($api_token, $limit);

    $status = $events_data->code;

    $logService = new \FGT\LogService($file);
    $logService->addToLog("Status is: $status");


    //If token is expired, get another token
    if($status == '401') {
        //echo 'getting new token';
        $_SESSION['fgt_api_token'] = $api->get_token();
        $api_token = isset($_SESSION['fgt_api_token']) ? $_SESSION['fgt_api_token'] : null;

        $logService->addToLog("API token is: $api_token");

        $events_data = $api->get_events($api_token);
    }

    $api_events = $events_data->body->data;
    $wp_events = get_posts( array( 'post_type' => 'events', 'numberposts' => -1 ) );

    $count = sizeof($api_events);

    $logService->addToLog($count);

    if($wp_events) {

        //Delete all events
        foreach ($wp_events as $event) {

            wp_delete_post($event->ID, true); //set to false to put in trash
        }
    }

    //for($i = 0; $i < 30; $i++) {
    for($i = 0; $i < $count; $i++) {

        $logService->addToLog($api_events[$i]->name);

        $new_wp_event = array(
            'post_title' => $api_events[$i]->name,
            'post_content' => $api_events[$i]->name,
            'post_status' => 'publish',
            'post_type' => 'events',
        );

        $new_wp_event_id = wp_insert_post($new_wp_event); //Returns id after insert

        //Normalize date
        //$date_arr = explode("T", $api_events[$i]->doorsStart, 2);
        //$the_date = $date_arr[0];
        //$normalized_date = str_replace('-', '', $the_date);

        $normalized_date = date('Ymd', strtotime( $api_events[$i]->doorsStart) );

        //Update post just created with "advanced custom fields" meta values
        update_field( 'venue', $api_events[$i]->venue->name, $new_wp_event_id );
        update_field( 'city', $api_events[$i]->city, $new_wp_event_id );
        update_field( 'state', $api_events[$i]->state, $new_wp_event_id );
        update_field( 'date', $normalized_date, $new_wp_event_id );
        update_field( 'image_url', $api_events[$i]->imageLargeUrl, $new_wp_event_id );
        update_field( 'event_url', $api_events[$i]->url, $new_wp_event_id );
        update_field( 'venue_url', $api_events[$i]->label->url, $new_wp_event_id );

    }
    if(function_exists('relevanssi_build_index')){
        relevanssi_build_index(); //Reindex the site
    }    

    $logService->writeLogToFile($logService->log);

}
