<?php

// Register Custom Post Type
function festivals_post_type() {

$labels = array(
'name'                  => _x( 'Festivals', 'Post Type General Name', 'festivals' ),
'singular_name'         => _x( 'Festival', 'Post Type Singular Name', 'festivals' ),
'menu_name'             => __( 'Festivals', 'festivals' ),
'name_admin_bar'        => __( 'Post Type', 'festivals' ),
'archives'              => __( 'Festival Archives', 'festivals' ),
'parent_item_colon'     => __( 'Parent Item:', 'festivals' ),
'all_items'             => __( 'All Festivals', 'festivals' ),
'add_new_item'          => __( 'Add New Festival', 'festivals' ),
'add_new'               => __( 'Add New', 'festivals' ),
'new_item'              => __( 'New Festival', 'festivals' ),
'edit_item'             => __( 'Edit Festival', 'festivals' ),
'update_item'           => __( 'Update Festival', 'festivals' ),
'view_item'             => __( 'View Festival', 'festivals' ),
'search_items'          => __( 'Search Festival', 'festivals' ),
'not_found'             => __( 'Not found', 'festivals' ),
'not_found_in_trash'    => __( 'Not found in Trash', 'festivals' ),
'featured_image'        => __( 'Featured Image', 'festivals' ),
'set_featured_image'    => __( 'Set featured image', 'festivals' ),
'remove_featured_image' => __( 'Remove featured image', 'festivals' ),
'use_featured_image'    => __( 'Use as featured image', 'festivals' ),
'insert_into_item'      => __( 'Insert into item', 'festivals' ),
'uploaded_to_this_item' => __( 'Uploaded to this item', 'festivals' ),
'items_list'            => __( 'Items list', 'festivals' ),
'items_list_navigation' => __( 'Items list navigation', 'festivals' ),
'filter_items_list'     => __( 'Filter items list', 'festivals' ),
);
$args = array(
'label'                 => __( 'Festival', 'festivals' ),
'description'           => __( 'Post Type Description', 'festivals' ),
'labels'                => $labels,
'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
'taxonomies'            => array( 'category', 'post_tag' ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'  		    => 'dashicons-format-audio',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => false,
'exclude_from_search'   => false,
'publicly_queryable'    => true,
'capability_type'       => 'page',
);
register_post_type( 'festivals', $args );

}
add_action( 'init', 'festivals_post_type', 0 );