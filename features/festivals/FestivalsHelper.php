<?php

class FestivalsHelper
{
    /*
     * This function allows us to store a custom template in a directory other than the wp root directory.
     * This helps siloh a feature by keeping its template in the same location as the other
     * feature-specific files.
     */
    static function get_festivals_single_template($single_template) {
        global $post;

        if ($post->post_type == 'festivals') {
            $single_template = dirname( __FILE__ ) . '/templates/single-festivals.php';
        }

        return $single_template;
    }

    static function get_festivals_archive_template( $template ){

        if( is_post_type_archive( 'festivals' ) ){
            $template = dirname( __FILE__ ) . '/templates/archive-festivals.php';
        }

        return $template;
    }
}
