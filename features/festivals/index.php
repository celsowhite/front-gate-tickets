<?php

//Register post type
require_once(get_template_directory().'/features/festivals/post-type-festivals.php');

//Include helper class
require_once(get_template_directory().'/features/festivals/FestivalsHelper.php');

//Use custom templates for this post type
add_filter( 'single_template', array('FestivalsHelper', 'get_festivals_single_template') );
//add_filter( 'template_include', array('FestivalsHelper', 'get_festivals_archive_template') );

//Register sidebar
function register_festivals_details_sidebar() {

    register_sidebar(array(
        'id' => 'festival_details',
        'name' => __('Festival Details', 'jointswp'),
        'description' => __('Festival Details Sidebar.', 'jointswp'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
}

add_action( 'widgets_init', 'register_festivals_details_sidebar' );


