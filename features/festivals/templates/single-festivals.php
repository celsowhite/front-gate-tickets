<?php get_header(); ?>

    <div id="content">

        <div class="row fgt-fdp-hero-outer">
            <div class="large-12 columns">
                <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="article-header">
                        <h1 class="entry-title single-title text-center" itemprop="headline">
                            <?php the_title(); ?>
                        </h1>

                        <div class="fgt-fdp-hero-wrapper">
                            <?php if(get_field('buy_tickets_url')): ?>
                                <a href="<?= get_field('buy_tickets_url'); ?>">
                                <?php the_post_thumbnail('full'); ?>
                            <?php endif; ?>

                            <?php if(get_field('buy_tickets_url')): ?>
                                </a>
                            <?php endif; ?>
                            <div class="caption text-center">
                                <?php

                                if(get_field('text_banner')) {
                                    echo get_field('text_banner');
                                }

                                ?>
                            </div>
                        </div>
                    </header> <!-- end article header -->
            </div>
        </div>

        <div id="inner-content" class="row">

            <main id="main" class="small-12 medium-8 columns" role="main">

                <div class="fgt-main-column">

                    <?php
                    //--------------------
                    // Media Content
                    //--------------------
                    if(get_field('media_content')) {
                        echo get_field('media_content');
                    }

                    //--------------------
                    // Buy Tickets Button
                    //--------------------
                    if(get_field('buy_tickets_url')): ?>
                        <a href="<?= get_field('buy_tickets_url'); ?>" class="full-width fgt-cta-2" target="_blank">BUY TICKETS</a>
                    <?php endif; ?>

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <div class="social_share" data-featured-image-url="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>"></div>
                        
                        <div class="article-main-content">
                            <section class="entry-content" itemprop="articleBody">
                                <?php the_content(); ?>
                            </section> <!-- end article section -->

                            <?php
                            //------------------
                            // BOTTOM CONTENT
                            //------------------
                            if(get_field('bottom_content')): ?>
                                <div class='small-12 columns'>

                                    <h4 class='fgt-widget-heading-inverse'>
                                        <?= get_field('bottom_content_title') ?>
                                    </h4>

                                    <div class="text-center"><?= get_field('bottom_content') ?></div>
                                </div>
                            <?php endif; ?>

                            <footer class="article-footer">
                                <p class="tags"><?php the_tags('<img src="' . get_template_directory_uri() . '/assets/images/icn-tag.png" /> ', ', ', ''); ?></p>
                            </footer> <!-- end article footer -->

                        </div>
                        
                    <?php endwhile; else: ?>

                        get_template_part( 'parts/content', 'missing' );

                    <?php endif; ?>

                </div>

                <!-- Related Posts -->
					
                <?php get_template_part('parts/related_posts'); ?>

            </main> <!-- end #main -->

            <div class="small-12 medium-4 columns">

                <!-----------------
                // Ad Space
                // --------------->
                <div class="small-12 columns text-center hide-for-small-only">
                    <div class="row fgt-widget-wrapper">
                        <div class="full-width">
                            <!-- 300x250/600 pos=502 -->
                            <div id='ad-slot-502'>
                                <script type='text/javascript'>
                                    googletag.cmd.push(function() { googletag.display('ad-slot-502'); });
                                </script>
                            </div>
                        </div>

                    </div>
                </div>

                <?php

                // ---------------
                // Spotify Player
                // ---------------

                if(get_field('spotify_link')) {
                    $embed_code = wp_oembed_get(get_field('spotify_link'));

                    // Fixes google Chrome bug: https://bugs.chromium.org/p/chromium/issues/detail?id=395533
                    $embed_code = str_replace("<iframe","<iframe name=\"".uniqid()."\"",$embed_code);

                    $template = "<div class='row'><div class='small-12 columns fgt-spotify-wrapper'>";
                    $template .= "<div class='fgt-widget-wrapper'>";
                    $template .= "<div class='text-center'>$embed_code</div>";
                    $template .= "</div></div></div>";

                    echo '<div class="hide-for-small-only">';
                    echo $template;
                    echo '</div>';
                }

                // --------------------------
                // Subscribe to email list
                // --------------------------
                echo '<div class="hide-for-small-only">';
                require_once(get_template_directory().'/features/email-subscribe/index.php');
                echo '</div>';

                // --------------------------
                // Popular Posts
                // --------------------------
                echo '<div class="hide-for-small-only">';
                require_once(get_template_directory().'/features/popular-posts/index.php');
                echo '</div>';

                // --------------------------
                // Follow Festival
                // --------------------------
                require_once(get_template_directory().'/features/festivals-social-media/index.php');

                // ------------------------
                // Tag Cloud
                // ------------------------
                require_once(get_template_directory().'/features/tag-cloud/index.php');

                ?>

            </div>



            <?php //get_sidebar('festival_details'); ?>

        </div> <!-- end #inner-content -->

        <div class="row fgt-leaderboard">
            <div class="small-12 columns text-center">                    
                <div id="ad-slot-501" class="leaderBoard">
                    <script type="text/javascript">
                        googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
                    </script>
                </div>
            </div>
        </div>

    </div> <!-- end #content -->

<?php get_footer(); ?>