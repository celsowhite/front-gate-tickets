<?php

// ------------------------
// News Article List
// ------------------------

$classes = get_body_class();

$is_festival_news_page = (in_array('blog',$classes)) ? true : false;

global $post;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$posts_per_page = 6;

$args	= array(
    'posts_per_page' => $posts_per_page,
    'paged'		 =>$paged,
    'post_type' => 'post',
    'order' => 'DESC',
    'orderby' => 'post_date',
    'post_status' => 'publish'
);

$articles = get_posts( $args ); ?>


<?php foreach($articles as $i => $post): ?>

    <?php

    setup_postdata( $post );

    $thumb_big = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $thumb_big_url = $thumb_big['0'];

    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
    $thumb_url = $thumb['0'];
    $excerpt = get_the_excerpt();

    $thumbURL = get_the_post_thumbnail_url($post->ID, 'medium');

    if(strlen($excerpt) > 155)
        $excerpt = preg_replace('/\s+?(\S+)?$/', '', substr($excerpt, 0, 155));

    ?>

    <?php if($i === 0): ?>
        <div class="small-12 columns">
            <div class="row fgt-news-box-main">

                <div class="medium-12 columns">
                    <div class="fgt-news-img-inner">
                        <a href="<?= get_permalink() ?>"><img src="<?= $thumb_big_url; ?>" /></a>
                    </div>

                    <a href="<?= get_permalink() ?>">
                        <h2 class="fgt-news-heading"><?= $post->post_title; ?></h2>
                    </a>

                    <p class="fgt-news-text">
                        <?php echo excerpt_trim(get_the_excerpt()); ?>
                        <span class="fgt-news-read-more">
                            <a href="<?= get_permalink() ?>" class="fgt-red">(Continue Reading)</a>
                        </span>
                    </p>

                </div>

            </div>
        </div>
    <?php else: ?>

        <div class="small-12 columns">

            <div class="row fgt-news-box">
                <a href="<?= get_permalink() ?>" class="fgt-news-image" style="background-image: url('<?php echo $thumbURL ?>')"></a>
                <div class="fgt-news-content">
                    <h2><a href="<?= get_permalink() ?>"><?= $post->post_title; ?></a></h2>
                    <p>
                        <?php echo excerpt_trim(get_the_excerpt()); ?>
                        <span class="fgt-news-read-more">
                        <a href="<?= get_permalink() ?>" class="fgt-red">(Continue Reading)</a>
                        </span>
                    </p>
                </div>
            </div>

        </div>

    <?php endif; ?>

<?php endforeach; ?>

<?php
wp_reset_postdata();

if ($is_festival_news_page) {
    echo '<img class="fgt-divider-margin" src="' . get_stylesheet_directory_uri() . '/assets/images/img-div-lg.png" />';

    //No pagination if the number of posts is not greater than the number of posts that should be on each page
    if( (int) wp_count_posts()->publish > $posts_per_page)
        fgt_pagination($wp_query->max_num_pages);
} ?>

<!-- Don't show view all button if on Festival News page -->
<?php if(!$is_festival_news_page): ?>

    <div class="row">
        <div class="small-12 columns">
            <a href="<?php echo get_permalink( get_page_by_path('festival-news')); ?>" class="fgt-cta-button expanded">VIEW ALL ARTICLES</a>
        </div>
    </div>

<?php endif; ?>

<?php
// ------------------------
// End News Article List
// ------------------------ ?>