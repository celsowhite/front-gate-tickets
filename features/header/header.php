<?php //session_start(); ?>

<!doctype html>

<html class="no-js"  <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta class="foundation-mq">

    <!-- If Site Icon isn't set in customizer -->
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <!-- Icons & Favicons -->
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
        <!--[if IE]>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <![endif]-->
        <meta name="msapplication-TileColor" content="#f01d4f">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">
    <?php } ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

    <!-- Drop Google Analytics here -->
    <!-- end analytics -->

    <!-- Krux Interchange Snippet Before Google -->

    <script>/*
        window.Krux||((Krux=function(){Krux.q.push(arguments);}).q=[]);
        function(retrieve(n){
            var(m,(k='kx'+n;
            (if((window.localStorage)({(
                return(window.localStorage[k](||("";
        }(else(if((navigator.cookieEnabled)({(
                m(=(document.cookie.match(k+'=([^;]*)');(
        return((m(&&(unescape(m[1]))(||("";
        }(else({(
        return('';
        }
        }
        Krux.user(=(retrieve('user');
        })();*/
    </script>

    <!-- Google code placed in header -->

    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>

    <script type='text/javascript'>

            googletag.cmd.push(function() {
            googletag.pubads().enableSingleRequest();
            googletag.pubads().enableAsyncRendering();
            googletag.pubads().collapseEmptyDivs();

            googletag.pubads().setTargeting("page","home");
            //googletag.pubads().setTargeting("kuid", Krux.user);

            // 728x90 970x66/90/250/418 mobile 300/320x50
            googletag.defineSlot('/6025/frontgate/home', [[970, 250], [970, 90], [970, 66], [728, 90], [300, 50], [320, 50]],'ad-slot-501')
                .defineSizeMapping(googletag.sizeMapping().
                    addSize([1024, 768],[[970, 250], [970, 90], [970, 66], [728, 90]]).addSize([800, 600],[728, 90]).addSize([0, 0],[[300, 50], [320, 50]]).build())
                .addService(googletag.pubads())
                .setTargeting("pagepos", "501");

            // 300x600/250 Desktop Tablet only
            googletag.defineSlot('/6025/frontgate/home', [[300, 600], [300, 250]],'ad-slot-502')
                .defineSizeMapping(googletag.sizeMapping().
                    addSize([1024, 768],[[300, 600], [300, 250]]).addSize([800, 600],[[300, 600], [300, 250]]).addSize([0, 0],[]).build())
                .addService(googletag.pubads())
                .setTargeting("pagepos", "502");

            // mobile adhesion
            googletag.defineSlot('/6025/frontgate/home', [[320, 50], [300, 50]],'ad-slot-690')
                .defineSizeMapping(googletag.sizeMapping().
                    addSize([1024, 768],[]).addSize([800, 600],[]).addSize([0, 0],[[320, 50], [300, 50]]).build())
                .addService(googletag.pubads())
                .setTargeting("pagepos", "690");

            //Custom code from Setlist to launch mobile adhesion unit
            googletag.pubads().addEventListener('slotRenderEnded', function(event) {

                if(event.slot.getSlotElementId() == '' && !event.isEmpty) {
                    var icon = $('<span class=\"fa-stack\"><i class=\"fa fa-circle fa-stack-2x\"><\/i><i class=\"fa fa-times fa-stack-1x fa-inverse\"><\/i><\/span>');
                    var sr = $('<span>', {
                        class: 'sr-only',
                        text: 'Close Advertisement',
                    });
                    var link = $('<a>',{
                        title: '"Close Advertisement',
                        class: 'closeLink',
                        href: '#',
                        click: function(){ $('#ad-slot-690').remove();return false;}
                    });
                    icon.appendTo(link);
                    sr.appendTo(link);
                    link.appendTo($('#ad-slot-690'));
                }});
            googletag.enableServices();
        });

    </script>


</head>

<!-- Uncomment this line if using the Off-Canvas Menu -->

<body <?php body_class(); ?>>

<!--
<div class="top-bar">
    <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">Site Title</li>
            <li class="has-submenu">
                <a href="#">One</a>
                <ul class="submenu menu vertical" data-submenu>
                    <li><a href="#">One</a></li>
                    <li><a href="#">Two</a></li>
                    <li><a href="#">Three</a></li>
                </ul>
            </li>
            <li><a href="#">Two</a></li>
            <li><a href="#">Three</a></li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
            <li><input type="search" placeholder="Search"></li>
            <li><button type="button" class="button">Search</button></li>
        </ul>
    </div>
</div>
<header class="fgt-header">

    <div class="container">
        <div class="row">
            <div class="small-4 columns">
                <a href="#">Front Gate Tickets</a>
            </div>
            <div class="small-2 columns">
                <a href="#">CUSTOMER SUPPORT</a>
            </div>
            <div class="small-2 columns">
                <a href="#">SELL TICKETS</a>
            </div>
            <div class="small-4 columns">
                SEARCH
            </div>
        </div>
    </div>

</header>

-->

<div class="off-canvas-wrapper">

    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <?php get_template_part( 'parts/content', 'offcanvas' ); ?>

        <div class="off-canvas-content" data-off-canvas-content>

            <header class="header" role="banner">

                <!-- This navs will be applied to the topbar, above all content
                     To see additional nav styles, visit the /parts directory -->

                <?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>

            </header> <!-- end .header -->

            <!--
            LEADER BOARD
            -->
            <!-- 728/970/300 pos=501 -->
<!--             <div class="row fgt-leaderboard">
                <div class="small-12 columns text-center">                    
                    <div id="ad-slot-501" class="leaderBoard">
                        <script type="text/javascript">
                            googletag.cmd.push(function() { googletag.display('ad-slot-501'); });
                        </script>
                    </div>
                </div>
            </div> -->


