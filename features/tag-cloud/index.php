<?php

require_once(get_template_directory().'/features/tag-cloud/TagCloud.php');

// -----------------
// Tag Cloud Markup
// -----------------

//shortcode
function fgt_tagcloud_shortcode( $atts ){

    $tagCloud = new TagCloud();
    $tags = $tagCloud->get_tags();

    $return = <<<EOT
    <div class='fgt-tag-cloud fgt-widget-margin-top fgt-widget-margin-bottom'>

        <div class="row">
            <div class="small-12 columns">
                <h4 class="fgt-widget-heading">TAG CLOUD</h4>
            </div>

        </div>

        <div class="row">

            <div class="small-12 columns">
EOT;

            if(sizeof($tags)) {
                foreach($tags as $tag) {
                    $return .= "$tag";
                }
            }

    $return .= <<<EOT
            </div>
        </div>
    </div>
EOT;

    return $return;
}

add_shortcode( 'fgt_tagcloud', 'fgt_tagcloud_shortcode' );

