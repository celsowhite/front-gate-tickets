<?php

//Without this, the tag cloud will bring in tags associated with custom post types,
//but the links generated for the tags will not include custom post type articles, only general posts.

add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
    if(is_category() || is_tag()) {
        $post_type = get_query_var('post_type');
        if($post_type)
            $post_type = $post_type;
        else
            $post_type = array('post','festivals'); // replace cpt to your custom post type
        $query->set('post_type',$post_type);
        return $query;
    }
}