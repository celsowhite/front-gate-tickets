<?php

class TagCloud
{

    public function get_tags($args = array('smallest' => 10, 'largest' => 10, 'orderby' => 'name', 'order' => 'ASC', 'format' => 'array')) {
        return wp_tag_cloud($args);
    }

}