<?php

function upcoming_festivals_list($args, $id){
 
  // Save yesterdays date so we can keep festivals on the site until after their start/end date has passed.

  $date_now = time();
  $date_yesterday = date('Y-m-d H:i:s', strtotime('-1 day', $date_now));

  // Query all upcoming festivals.
  // Order the festivals by ascending start date.

  $args['post_type'] = 'festivals';
  $args['meta_query'] = array(
    'relation'      => 'OR',
    array(
        'key'       => 'date',
        'value'     => $date_yesterday,
        'compare'   => '>',
        'type'      => 'DATE'
    ),
    array(
        'relation'  => 'AND',
        array(
            'key'       => 'end_date',
            'compare'   => 'EXISTS'
        ),
        array(
            'key'       => 'end_date',
            'value'     => $date_yesterday,
            'compare'   => '>',
            'type'      => 'DATE'
        )
    )
  );
  $args['meta_key'] = 'date';
  $args['orderby']  = 'meta_value';
  $args['order']    = 'ASC';

  return $args;
 
}

add_filter( 'alm_query_args_upcoming_festivals', 'upcoming_festivals_list', 10, 2);

?>