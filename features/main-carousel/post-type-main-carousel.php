<?php

// Register Custom Post Type
function carousel_images_post_type() {

    $labels = array(
        'name'                  => _x( 'Carousel Images', 'Post Type General Name', 'fgt' ),
        'singular_name'         => _x( 'Carousel Image', 'Post Type Singular Name', 'fgt' ),
        'menu_name'             => __( 'Carousel Images', 'fgt' ),
        'name_admin_bar'        => __( 'Post Type', 'fgt' ),
        'archives'              => __( 'Carousel Image Archives', 'fgt' ),
        'parent_item_colon'     => __( 'Parent Item:', 'fgt' ),
        'all_items'             => __( 'All Carousel Images', 'fgt' ),
        'add_new_item'          => __( 'Add New Carousel Image', 'fgt' ),
        'add_new'               => __( 'Add New', 'fgt' ),
        'new_item'              => __( 'New Carousel Image', 'fgt' ),
        'edit_item'             => __( 'Edit Carousel Image', 'fgt' ),
        'update_item'           => __( 'Update Carousel Image', 'fgt' ),
        'view_item'             => __( 'View Carousel Image', 'fgt' ),
        'search_items'          => __( 'Search Carousel Image', 'fgt' ),
        'not_found'             => __( 'Not found', 'fgt' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'fgt' ),
        'featured_image'        => __( 'Featured Image', 'fgt' ),
        'set_featured_image'    => __( 'Set featured image', 'fgt' ),
        'remove_featured_image' => __( 'Remove featured image', 'fgt' ),
        'use_featured_image'    => __( 'Use as featured image', 'fgt' ),
        'insert_into_item'      => __( 'Insert into item', 'fgt' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'fgt' ),
        'items_list'            => __( 'Items list', 'fgt' ),
        'items_list_navigation' => __( 'Items list navigation', 'fgt' ),
        'filter_items_list'     => __( 'Filter items list', 'fgt' ),
    );

    $args = array(
        'label'                 => __( 'Carousel Image', 'fgt' ),
        'description'           => __( 'Images are for the carousel. Excerpt content is the caption', 'fgt' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'  		    => 'dashicons-format-gallery',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );

    register_post_type( 'carousel_images', $args );

}
add_action( 'init', 'carousel_images_post_type', 0 );