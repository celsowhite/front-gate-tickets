<?php

$args = array(
    "orderby"     => 'meta_value_num',
    "meta_key"    => 'index',
    "order"       => 'ASC',
    'post_type'   => 'carousel_images',
    'post_status' => 'publish'
);

$image_objects = get_posts($args);
$class = 'hide';
$template = '';


foreach($image_objects as $key => $post) {
    setup_postdata( $post );

    $class = ($key === 0) ? "is-active " : "hide ";
    $img_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
    $caption = get_field('caption');
    $link = get_field('link');
    $template .= <<<EOT

    <li class="$class orbit-slide">
        <div class="text-center">
            <a href="$link">
                <img src="$img_url" />
            </a>
            <div class="carousel-caption">
                $caption
            </div>

        </div>
    </li>

EOT;

} ?>

<div class="row fgt-main-carousel">

    <div class="small-12 columns">
        <div class="orbit" role="region" aria-label="Favorite Space Pictures">

            <ul class="orbit-container">
                <button class="orbit-previous hide-for-small-only" aria-label="previous"><span class="show-for-sr">Previous Slide</span><div class="arrow-left"></div></button>
                <button class="orbit-next hide-for-small-only" aria-label="next"><span class="show-for-sr">Next Slide</span><div class="arrow-right"></div></button>
                <?= $template ?>
            </ul>

        </div>
    </div>

</div>