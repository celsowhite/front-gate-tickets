<?php header('Content-type: application/json');
require_once('../../services/EmailSubscribeHelper.php');

//Stop execution and throw exception if no email set
if(!isset($_POST['email']))
    throw new Exception("Email address not set");

$email = $_POST['email'];

$emailSubscribe = new EmailSubscribeHelper();
$emailSubscribe->authenticate();

$res = $emailSubscribe->addMember($email);

echo json_encode($res);
