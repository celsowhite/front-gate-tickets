<?php require_once(get_template_directory().'/services/EmailSubscribeHelper.php'); ?>



<div class="fgt-email-subscribe fgt-email-subscribe-small">

    <div class="row">

        <div class="columns"><img src="<?php echo get_stylesheet_directory_uri() . '/features/email-subscribe/images/img-sub-sm.png' ?>" /></div>

        <div class="small-9 columns fgt-email-subscribe-input">
            <form class="email-subscribe-form">
                <div class="input-group">
                    <input class="input-group-field email-address" type="text" placeholder="SUBSCRIBE TO FGT">
                    <div class="input-group-button">
                        <input type="submit" class="button inline-button-fix" value="SIGN UP">
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>
