<?php require_once(get_template_directory().'/services/EmailSubscribeHelper.php'); ?>

<div class="fgt-email-subscribe fgt-email-subscribe-large">

    <div class="row">
        <div class="small-1 large-1 columns"></div>

        <div class="small-10 large-10 columns fgt-email-subscribe-content">
            <div class="row">
                <div class="small-12 large-3 columns"><img src="<?php echo get_stylesheet_directory_uri() . '/features/email-subscribe/images/img-sub-lg.png' ?>" /></div>
                <div class="small-12 large-9 columns fgt-email-subscribe-input">
                    <h3 class="hide-for-small-only">DISCOVER NEW ARTISTS / NEVER MISS A SHOW</h3>
                    <h3 class="show-for-small-only mobile-email-subscribe-header">DISCOVER NEW ARTISTS<br>NEVER MISS A SHOW</h3>

                    <div class="hide-for-small-only">
                        
                        <form class="email-subscribe-form">
                            <div class="input-group">
                                <input class="input-group-field email-address" type="text" placeholder="SUBSCRIBE TO THE FRONT GATE NEWSLETTER">
                                <div class="input-group-button">
                                    <input type="submit" class="button inline-button-fix" value="SIGN UP">
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

            </div>

        </div>

        <div class="small-1 large-1 columns"></div>

        <div class="small-12 columns text-centered show-for-small-only">
            <a class="button fgt-cta-button full-width mobile-sign-up-button" href="#">SIGN UP</a>

            <div class="mobile-sign-up-form">
                <fieldset class="fieldset">
                    <form class="email-subscribe-form">
                        <input type="text" class="email-address" placeholder="Enter Email" />
                        <a href="#" class="button full-width fgt-cta-button mobile-subscribe-button">SUBSCRIBE</a>
                    </form>
                </fieldset>
            </div>

        </div>

    </div>

</div>
