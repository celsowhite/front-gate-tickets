<?php

/* // Register Custom Post Type
function events_post_type() {

$labels = array(
'name'                  => _x( 'Events', 'Post Type General Name', 'fgt' ),
'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'fgt' ),
'menu_name'             => __( 'Events', 'fgt' ),
'name_admin_bar'        => __( 'Post Type', 'fgt' ),
'archives'              => __( 'Event Archives', 'fgt' ),
'parent_item_colon'     => __( 'Parent Item:', 'fgt' ),
'all_items'             => __( 'All Events', 'fgt' ),
'add_new_item'          => __( 'Add New Event', 'fgt' ),
'add_new'               => __( 'Add New', 'fgt' ),
'new_item'              => __( 'New Event', 'fgt' ),
'edit_item'             => __( 'Edit Event', 'fgt' ),
'update_item'           => __( 'Update Event', 'fgt' ),
'view_item'             => __( 'View Event', 'fgt' ),
'search_items'          => __( 'Search Event', 'fgt' ),
'not_found'             => __( 'Not found', 'fgt' ),
'not_found_in_trash'    => __( 'Not found in Trash', 'fgt' ),
'featured_image'        => __( 'Featured Image', 'fgt' ),
'set_featured_image'    => __( 'Set featured image', 'fgt' ),
'remove_featured_image' => __( 'Remove featured image', 'fgt' ),
'use_featured_image'    => __( 'Use as featured image', 'fgt' ),
'insert_into_item'      => __( 'Insert into item', 'fgt' ),
'uploaded_to_this_item' => __( 'Uploaded to this item', 'fgt' ),
'items_list'            => __( 'Items list', 'fgt' ),
'items_list_navigation' => __( 'Items list navigation', 'fgt' ),
'filter_items_list'     => __( 'Filter items list', 'fgt' ),
);
$args = array(
'label'                 => __( 'Event', 'fgt' ),
'description'           => __( 'Data from events API is inserted into this post type', 'fgt' ),
'labels'                => $labels,
'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
'taxonomies'            => array( 'category', 'post_tag' ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => true,
'exclude_from_search'   => false,
'publicly_queryable'    => true,
'capability_type'       => 'page',
);
register_post_type( 'events', $args );

}
add_action( 'init', 'events_post_type', 0 ); */