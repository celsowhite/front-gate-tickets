<?php require_once(get_template_directory().'/services/EventsHelper.php'); ?>

<!-- Heading -->
<div class="fgt-events">

    <div class='row'>
        <div class='small-12 columns'>
            <h2 class='fgt-page-heading'>EVENTS</h2>
        </div>
    </div>

    <!-- Filter/Search Options -->
    <div class="hide-for-small-only events-search-filter">
        <?php echo get_search_form(false); ?>
    </div>


    <div class='row'>

        <?php

        //Events
        //$event_args = ['posts_per_page' => 10];
        //$festival_args = ['posts_per_page' => 2];

        $all_events = EventsHelper::getAllEvents($limit = 12);

        global $post;

        $limit_mobile = 5;
        $i = 1;
        foreach($all_events as $post) {

            $class = ($i >  $limit_mobile) ? 'hide-for-small-only' : '';

            setup_postdata($post);

            $view_model = EventsHelper::setupViewModel();
            extract($view_model); ?>

            <div class="event-block small-12 medium-4 large-4 columns <?= $class ?>" data-equalizer-watch>

                <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

                    <section class="featured-image" itemprop="articleBody">

                        <a href="<?= $event_url ?>" target="_blank"><img src="<?= $thumb ?>" /></a>

                    </section> <!-- end article section -->

                    <div class="event-info-block">
                        <header class="article-header">
                            <h4 class="title"><a href="<?= $event_url ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" target="_blank"><?php the_title(); ?></a></h4>
                            <span class='event-time'><?= $date ?></span>

                            <div class='event-location'>
                                <span><a href="<?= $venue_url ?>" target="_blank"><?= $venue ?></a></span>
                                <span class='float-right'><?= $city ?>, <?= $state ?></span>
                            </div>
                        </header> <!-- end article header -->
                    </div>

                </article> <!-- end article -->

            </div>

        <?php $i++; } //end foreach ?>

        <?php wp_reset_postdata();
        ?>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <a href="/?s=&pagetitle=All Events" class="fgt-cta-button expanded">VIEW ALL EVENTS</a>
            <img class="fgt-divider-margin" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-div-lg.png' ?>" />
        </div>
    </div>
</div>


