<?php

require_once(get_template_directory().'/vendor/unirest/src/Unirest.php');
require_once(get_template_directory().'/features/events/EventsAPIHelper.php');



class EventsView {
    public function getTemplate($events) {

        $event_template = '';

        $events_template = <<<HTML
            <div class='row'>
                <div class='small-12 columns'>
                    <h2 class='fgt-page-heading'>EVENTS</h2>
                </div>
            </div>
            <div class='row'>
HTML;



        foreach($events as $event) {

            if($event->active) {

                $event_time = date('F j', $event->doorsStart);

                $event_template = "<div class='event-block medium-4 small-12 columns'>";
                $event_template .= "<img src='{$event->imageUrl}' /><br />";
                $event_template .= "<div class='event-info-block'>";
                $event_template .= "<h4>{$event->name}</h4>";
                $event_template .= "<span class='event-time'>$event_time</span>";
                $event_template .= "<div class='event-location'>";
                $event_template .= "<span>{$event->venue->name}</span>";
                $event_template .= "<span class='float-right'>{$event->city}, {$event->state}</span>";
                $event_template .= "</div></div></div>";
            }

            $events_template .= $event_template;
        }

        $events_template .= "</div>";

        return $events_template;
    }
}




class EventsAPIHelper
{

    private $username = 'ecabc0590e48845a6c2eb3067cf2b895';
    private $password = '9d7b5f402a29e9ac32459b640f27c4ca';
    private $token = null;

    public function get_token() {


    }

    public function get_events( $token ) {


    }

}