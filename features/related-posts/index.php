<?php

//Include helper class
require_once(get_template_directory().'/features/related-posts/RelatedPosts.php');

$relatedPosts = new RelatedPosts();

$num_related = 20;


//$relatedPostsCollection = $relatedPosts->getPosts($post->ID, $num_related);


$args = array(
    //'post_types'     => array('post', 'events', 'festivals'),      // string or array with multiple post type names
    'post_types'     => array('post'),
    'posts_per_page' => 20,           // return 5 posts
    'order'          => 'DESC',      // 'DESC', 'ASC' or 'RAND'
    'orderby'        => 'post_date', // 'post_date' or 'post_modified'
    'exclude_terms'  => '',          // string or array with term IDs
    'include_terms'  => '',          // string or array with term IDs
    'exclude_posts'  => '',          // string or array with post IDs
    'limit_posts'    => -1,          // don't limit posts
    'limit_year'     => '',          // number of years
    'limit_month'    => '',          // number of months
    'fields'         => 'all',       // 'all' (return post objects), 'ids', 'names' or 'slugs'
    'related'        => 1,           // (bool) search in related terms for the current post ID
    'post_thumbnail' => 0,           // (bool) return only posts with a post thumbnail.
);
$relatedPostsCollection = $relatedPosts->km_rpbt_related_posts_by_taxonomy($post->ID, array( 'category','post_tag'), $args);


?>

<?php if($relatedPostsCollection): ?>
<div class="fgt-related-posts">

    <h4 class="text-center">RELATED ARTICLES</h4>

    <div class="related-items">

<?php

$i = 1;

    global $post;

    foreach($relatedPostsCollection as $key => $post): ?>

        <?php

        setup_postdata($post);

        $url = wp_get_attachment_url( get_post_thumbnail_id($relatedPost->ID, 'thumbnail') );

        $excerpt = $relatedPosts->trimExcerpt(100);
        ?>

        <?php if($key === 0): ?>


        <div class="related-main-wrapper">
            <div class="row fgt-related-row related-main">
                <div class="small-4 medium-4 columns">
                    <div class="fgt-related-image-wrapper">
                        <img src="<?= $url ?>" />
                    </div>

                </div>
                <div class="small-8 medium-8 columns">
                    <div class="fgt-related-content-wrapper">
                        <a href="<?= get_permalink($post->ID) ?>">
                            <h5><?= $post->post_title ?></h5>
                        </a>
                        <div><?= $excerpt ?></div>
                    </div>

                </div>
            </div>
        </div>


        <?php else: ?>

            <?php if($key % 2 !== 0): ?>

                <?php $related_class = 'related-left'; ?>
                <div class="related-wrapper">
                <div class="row fgt-related-row related-sub">
                <?php else: ?>
                <?php $related_class = 'related-right' ?>
            <?php endif; ?>

                <div class="small-12 medium-6 columns">

                    <div class="row <?= $border_class ?>">
                        <div class="small-12 columns <?= $related_class ?>">
                            <div class="related-sub-wrapper">
                                <div class="row">
                                    <div class="small-5 medium-5 columns <?= $padding_class; ?>">
                                        <div class="fgt-table-display">
                                            <div class="fgt-related-image-wrapper">
                                                <img src="<?= $url ?>" />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="small-7 medium-7 columns">
                                        <div class="fgt-table-display">
                                            <div class="fgt-related-content-wrapper">
                                                <a href="<?= get_permalink($post->ID) ?>">
                                                    <div><?= $post->post_title ?></div>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            <?php if($key % 2 == 0 || $i === sizeof($relatedPostsCollection) ): ?>
                </div>
                </div>
            <?php endif; ?>

        <?php endif; ?>


    <?php

    $i++;

    endforeach;
    wp_reset_postdata();
    ?>
    </div> <!-- end related items -->

    <?php if(sizeof($relatedPostsCollection) > 3): ?>
        <div class="row">
            <div class="small-12 columns">
                <a href="#" class="button fgt-related-cta full-width">VIEW ALL</a>
            </div>
        </div>

    <?php endif; ?>



</div>

<?php endif; ?>




