<?php

/*--------------------
Filter Search Results
---
Ensure festivals appear first and no past festivals appear.
--------------------*/

add_filter('relevanssi_hits_filter', 'events_search_filter');

function events_search_filter($hits) {

    // First sort all of our results so Festivals appear first and in order of ascending start date.

    usort($hits[0], function($a, $b) {

        // Set our post type order hierarchy. Festivals are prioritized.

        $post_types = [
            'festivals' => 1,
            'post' => 2
        ];

        // If post types are different for the two posts then make sure to order the festival first.

        if($post_types[$a->post_type] != $post_types[$b->post_type]) {
            return $post_types[$a->post_type] - $post_types[$b->post_type];
        }

        // Else post types are the same then order them by ascending date.

        else {
            // Festivals are in ascending order by their start date.
            if($a->post_type == 'festivals') {
                return get_field('date', $a->ID) > get_field('date', $b->ID);
            }
            // Posts are in descending order by date posted. So newest posts appear first.
            else {
                return $a->post_date < $b->post_date;
            }
        }

    });

    // Next remove any festivals whose date has already passed

    $filtered_hits = array_filter($hits[0], function($value) {

        if($value->post_type == 'festivals') {

            $date_now = time();
            $date_yesterday = date('Y-m-d', strtotime('-1 day', $date_now));

            // If end date exists then return the festival if that end date hasn't passed.

            if(get_field('end_date', $value->ID)) {
                return get_field('end_date', $value->ID) > $date_yesterday;
            }

            // Else no end date exists then the festival only has a start date. Check to see if that start date hasn't passed.

            else {
                return get_field('date', $value->ID) > $date_yesterday;
            }

        }
        else {
            return true;
        }

    });

    // Have to call array_values on the filtered array so it resets the keys.
    // array_filter preserves array keys which messes up relevanssi output.

    $hits[0] = array_values($filtered_hits);

    return $hits;

}

/*--------------------
Add Festival Custom Fields to Content for indexing
--------------------*/

add_filter('relevanssi_post_content', 'custom_fields_to_content', 10, 2);

function custom_fields_to_content($content, $post) {

	if($post->post_type == 'festivals') {
        $city = get_field('city', $post->ID);
        $state = get_field('state', $post->ID);
        $venue = get_field('venue', $post->ID);
        $bottom_content = get_field('bottom_content', $post->ID);
    	$content .= " " . $city . ' ' . $state . ' ' . $venue . ' ' . $bottom_content;
    }

    return $content;

}

?>