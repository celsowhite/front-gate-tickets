<?php
/**
 * Template Name: Festival News
 *
 * @package WordPress
 * @subpackage Joints
 */

get_header(); ?>

<div id="content">

	<div class="row">
		<div class="large-12 columns">
			<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

				<header class="article-header">
					<h1 class="entry-title single-title text-center" itemprop="headline">
						FESTIVAL NEWS
					</h1>

					<?php get_template_part( 'parts/content', 'byline' ); ?>
				</header> <!-- end article header -->
			</article>
		</div>
	</div>

	<div id="inner-content" class="row">

		<main id="main" class="large-8 medium-8 columns" role="main">

			<?php
			// --------------------------
			// News List
			// --------------------------
			require_once(get_template_directory().'/features/news-list/index.php'); ?>


		</main> <!-- end #main -->

		<div class="large-4 medium-4 columns">
		
			<?php
			// -----------------
			// Categories
			// -----------------
			echo do_shortcode( '[fgt_categories]' ); ?>
			
			<!-----------------
            // Ad Space
            // --------------->
			<div class="small-12 columns text-center">
				<div class="row fgt-widget-wrapper">
					<div class="full-width">
						<!-- 300x250/600 pos=502 -->
						<div id='ad-slot-502'>
							<script type='text/javascript'>
								googletag.cmd.push(function() { googletag.display('ad-slot-502'); });
							</script>
						</div>
					</div>

				</div>
			</div>

			<?php
			// --------------------------
			// Popular Posts
			// --------------------------
			require_once(get_template_directory().'/features/popular-posts/index.php');


			// --------------------------
			// Subscribe to email list
			// --------------------------
			require_once(get_template_directory().'/features/email-subscribe/index.php');

			?>

		</div>



		<?php //get_sidebar('festival_details'); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>

<!--


-->
