<?php

require_once(dirname(__FILE__) . '/../vendor/unirest/src/Unirest.php');

class EmailSubscribeHelper
{

    // private $username = 'crispy';
    // private $password = 'e2e8a8543e3ce4da67116a680eb9efb1-us10';
    // private $root = 'https://us10.api.mailchimp.com/3.0';
    private $username = 'ubogler';
    private $password = 'e90fb1984a52298b4109cdbf7bc75a13-us8';
    private $root = 'https://us8.api.mailchimp.com/3.0';

    public function authenticate() {

        Unirest\Request::auth($this->username, $this->password);

        //$auth_url = 'https://us10.api.mailchimp.com/3.0/';
        $auth_headers = array("Accept" => "application/json");

        $auth_response = Unirest\Request::get( $this->root, $auth_headers );

        return $auth_response;
    }

    public function addMember($email) {

        $subscriber_hash = md5(strtolower($email));

        $headers = array("Accept" => "application/json");
        //$list_endpoint = "{$this->root}/lists/d836cbfcaa/members/";
        $list_endpoint = "{$this->root}/lists/e47d99139a/members/";
        $body = array("email_address" => "$email", "status" => "subscribed");
        $body = json_encode($body);

        return Unirest\Request::post( $list_endpoint, $headers, $body );
    }

}