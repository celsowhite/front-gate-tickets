<?php
/**
 * Created by PhpStorm.
 * User: robluton
 * Date: 1/15/16
 * Time: 10:03 AM
 */

namespace FGT;


class LogService
{

    public $file;
    public $log = '';

    public function __construct($file_path) {
        $this->file = $file_path;
    }

    public function writeLogToFile($log, $append = true) {
        $date = date('m/d/y h:i:s');
        $existing_content = file_get_contents($this->file);
        $new_content = "------------------------------------\n";
        $new_content .= "New Log Entry: $date";
        $new_content .= "\n------------------------------------";
        $new_content .= "$log\n\n";
        $content = $new_content . $existing_content;
        file_put_contents($this->file, $content);
    }

    public function addToLog($message) {
        $this->log .= "\n$message\n";
    }

    public function clearLog() {
        $this->log = '';
    }

}