<?php

class EventsAPIHelper
{

    private $username = 'ecabc0590e48845a6c2eb3067cf2b895';
    private $password = '9d7b5f402a29e9ac32459b640f27c4ca';
    private $token = null;

    public function get_token() {

        Unirest\Request::auth($this->username, $this->password);

        $auth_url = 'https://api.frontgatetickets.com/v1/access-token';
        $auth_headers = array("Accept" => "application/json");

        $auth_response = Unirest\Request::post( $auth_url, $auth_headers );

        return $auth_response->body->data->token;
    }

    public function get_events( $token, $limit = '' ) {

        (isset($limit)) ? $limit_param = "&limit=$limit" : $limit_param = '';


        $headers = array("Accept" => "application/json", "Authorization" => "Bearer " . $token);
        $events_endpoint = "http://api.frontgatetickets.com/v1/event?portal=1$limit_param";

        //$wristbands_endpoint = 'http://api.frontgatetickets.com/v1/site/219/wristband?registered=1&limit=3';

        return $response = Unirest\Request::get( $events_endpoint, $headers );

    }

}