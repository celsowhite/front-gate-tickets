<?php

class EventsHelper
{

    private static $all_events_args = array('posts_per_page' => -1, 'offset' => 0, 'meta_key'=>'date', 'orderby' => 'meta_value_num', 'order' => 'ASC', 'post_type' => array('festivals', 'events'), 'post_status' => 'publish', 'suppress_filters' => true);
    private static $events_args = array('posts_per_page' => -1, 'offset' => 0, 'orderby' => 'date', 'order' => 'ASC', 'post_type' => 'events', 'post_status' => 'publish', 'suppress_filters' => true);
    private static $festivals_args = array('posts_per_page' => -1, 'offset' => 0, 'orderby' => 'date', 'order' => 'ASC', 'post_type' => 'festivals', 'post_status' => 'publish', 'suppress_filters' => true);

    static function getAllEvents($limit = null, $args = null) {

        if( is_null($args) ) {
            $args = self::$all_events_args;
        } else {
            $args = array_merge(self::$all_events_args, $args);
        }

        $all_events = self::hoist_festivals(get_posts($args));

        if( isset($limit) )
            $all_events = array_slice($all_events, 0, $limit);

        return $all_events;

    }

    static function getEvents( $args = null ) {

        if( is_null($args) ) {
            $args = self::$events_args;
        } else {
            $args = array_merge(self::$events_args, $args);
        }

        return get_posts($args);
    }

    static function getFestivals( $args = null ) {

        if( is_null($args) ) {
            $args = self::$festivals_args;
        } else {
            $args = array_merge(self::$festivals_args, $args);
        }

        return get_posts($args);
    }

    static function isFestival() {
        $post_type = get_post_type( get_the_ID() );

        return (strtolower($post_type) === 'festivals');
    }

    //Must be inside the wp loop
    static function setupViewModel() {

        $is_festival = self::isFestival();

        $view_vars = [];
        $view_vars['post_type'] = get_post_type( get_the_ID() );
        $view_vars['is_festival'] = (strtolower($post_type) === 'festivals');
        $view_vars['thumb'] = $is_festival ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0] : get_field('image_url');
        $view_vars['city'] = $is_festival ? get_field('city') : get_field('city');
        $view_vars['state'] = $is_festival ? get_field('state') : get_field('state');
        $view_vars['venue'] = $is_festival ? get_field('venue') : get_field('venue');
        $view_vars['date'] =  $is_festival ? date('F j', strtotime(get_field('date'))) : date('F j', strtotime(get_field('date')));
        $view_vars['event_url'] = $is_festival ? get_permalink() : get_field('event_url');
        $view_vars['venue_url'] = $is_festival ? null : get_field('venue_url');

        return $view_vars;
    }

    static function getFiltersTemplate() {

        $search_form = get_search_form(false);

        return $search_form;
    }



    //Client wants first row to always be festivals
    static function hoist_festivals($sorted_hits) {

        $top_row = [];
        $i = 0;

        foreach($sorted_hits as $sorted_hit) {

            if(get_post_type($sorted_hit) === 'festivals') {

                array_push($top_row, $sorted_hit);
                unset($sorted_hits[$i]);
                //$sorted_hits = array_values($sorted_hits);
                if(sizeof($top_row) == 3) {
                    break;
                }

            }
            $i++;
        }

        //var_dump($top_row); die();
        for($i = count($top_row) - 1; $i >= 0; $i--) {

            //var_dump($top_row[$i]);
            array_unshift($sorted_hits, $top_row[$i]);
        }

        return $sorted_hits;
    }



}