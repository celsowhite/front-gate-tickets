<?php
/**
 * Created by PhpStorm.
 * User: robluton
 * Date: 1/11/16
 * Time: 4:09 PM
 */

namespace FGT;


class UrlHelper

{
    /**
     * Returns the current URL.
     *
     * @param array $url_config used to override default options, such as whether or not
     * to include parameters in the returned url
     *
     * @return $url
     */

    static function getURL($url_config = ['include_params' => true]) {

        $url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

        if(!$url_config['include_params'])
            return strtok($url,'?'); //return url without parameters

        //return full url
        return $url;

    }

}