<?php
/**
 * Created by PhpStorm.
 * User: robluton
 * Date: 1/11/16
 * Time: 4:09 PM
 */

namespace FGT;


class Paginate
{

    private $page_num = 1;
    private $per_page = 12;

    public function __construct($per_page, $page_num) {
        $this->page_num = $page_num;
        $this->per_page = $per_page;
    }

    public function getFirstIndex() {
        return ($this->page_num > 1) ? ($this->per_page * $this->page_num) - $this->per_page : 0;
    }

    public function getLastIndex() {
        return $this->getFirstIndex() + $this->per_page;
    }

    public function getNumPages($arr) {
        $total = sizeof($arr);
        return ceil($total / $this->per_page);
    }

    public function getURL() {
        $url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

        //remove url parameters
        return strtok($url,'?');
    }

    public function createPageLinks($arr) {
        $num_pages = $this->getNumPages($arr);
        $current_url = $this->getURL();
        $template = "<div class='fgt-paginated-links'><span class='prev'></span>";

        for( $i = 1; $i <= $num_pages; $i++ ) {
            $template .= "<a href='$current_url?page_no=$i'>$i</a>";
        }

        $template .= "<span class='next'></span></div>";

        return $template;
    }

    public function paginateArray($arr) {

        $first = $this->getFirstIndex();
        $last = $this->getLastIndex();

        return array_slice($arr, $first, $this->per_page);
    }

}