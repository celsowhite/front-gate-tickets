<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->
<div class="top-bar-wrapper">
	<div class="row">
		<div class="top-bar" id="top-bar-menu" style="width: 100%;">
			<div class="row">

				<div class="small-7 medium-3 columns">
					<div class="top-bar-left">
						<ul class="menu">
							<li class="menu-text">
								<a href="<?php echo home_url(); ?>">
									<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-fgt-nav.png' ?>" class="top-logo"/>
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="small-1 medium-5 columns show-for-medium">
					<div class="show-for-medium">
						<?php

						$backup = $wp_query;
						$wp_query = NULL;
						$wp_query = new WP_Query(array('post_type' => 'post'));

						joints_top_nav();

						$wp_query = $backup;

						?>
					</div>
				</div>

				<div class="small-5 medium-4 columns">

					<?php include TEMPLATEPATH . '/parts/search-top.php' ?>

					<div class="top-bar-right float-right show-for-small-only">
						<ul class="menu">
							<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
							<li><a data-toggle="off-canvas"><span class="menu-icon"></span></a></li>
						</ul>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>


