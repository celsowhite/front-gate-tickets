<?php 
/* --------------------------
Related Posts
---
Used in single and festival post types. Ability for FGT team to manually curate related posts using ACF.
Or we auto generate related posts using tags.
--------------------------*/

// If manually curating related posts then show those posts.

$related_posts = get_field('related_posts'); if($related_posts): ?>

    <div class="related_posts_container">
    
        <h4 class="text-center">Related Festivals & Features</h4>

        <div class="fgt-events">

            <div class="search-grid">
                
                <?php foreach( $related_posts as $post ): ?>

                    <?php setup_postdata($post); ?>
                            
                    <?php get_template_part( 'parts/loop', 'search-results' ); ?>
                        
                <?php endforeach; wp_reset_postdata(); ?>

            </div>

        </div>

    </div>

<?php 
// Else no related posts were manually added. Then attempt to intelligently generate related posts through tags.
else: ?>

    <?php 
    // If this post has tags then show related posts by tag.
    $tags = wp_get_post_tags($post->ID);
    if($tags):
    ?>

        <?php
        $tag_ids = array();
        foreach($tags as $individual_tag) {
            $tag_ids[] = $individual_tag->term_id;
        }
        $date_now = date('Y-m-d H:i:s');
        $related_posts_args = array (
            'post_type'      => array('post', 'festivals'), 
            'tag__in'        => $tag_ids,
            'post__not_in'   => array($post->ID),
            'posts_per_page' => 3,
            'meta_query'     => array(
                'relation'   => 'OR',
                // Include posts that don't have the date metafield.
                array(
                    'key'       => 'date',
                    'compare'   => 'NOT EXISTS'
                ),
                // Ensure festivals only appear if their date has not yet past.
                array(
                    'key'       => 'date',
                    'value'     => $date_now,
                    'compare'   => '>=',
                    'type'      => 'DATE'
                )
            )
        );
        $related_posts_loop = new WP_Query($related_posts_args);
        if ($related_posts_loop -> have_posts()) :
        ?>	
            <div class="related_posts_container">
                <h4 class="text-center">Related Festivals & News</h4>
                <div class="fgt-events">
                    <div class="search-grid">
                        <?php while ($related_posts_loop -> have_posts()) : $related_posts_loop -> the_post(); ?>
                            <?php get_template_part( 'parts/loop', 'search-results' ); ?>
                        <?php endwhile; wp_reset_postdata() ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    <?php endif; ?>

<?php endif; ?>