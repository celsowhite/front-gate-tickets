<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	<header class="article-header">
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

		<div class="row">
			<div class="columns">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/frontgate-logo.png' ?>" />
			</div>
			<div class="small-11 columns no-padding-left">
				<p class="byline">
					<span class="fgt-byline">Front Gate Tickets</span>
					<?php the_time('F j, Y') ?>
				</p>
			</div>
		</div>

	</header> <!-- end article header -->
					
	<section class="entry-content" itemprop="articleBody">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
		<?php the_content('<button class="tiny">Read more...</button>'); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<p class="tags"><?php the_tags('<img src="' . get_template_directory_uri() . '/assets/images/icn-tag.png" /> ', ', ', ''); ?></p>	</footer> <!-- end article footer -->

	</footer> <!-- end article footer -->
</article> <!-- end article -->