<div class="event-block" data-equalizer-watch>

	<a href="<?php the_permalink(); ?>" class="featured-image" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');" itemprop="articleBody">

	</a> 

	<div class="event-info-block">
		<header class="article-header">

			<h4 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			
			<?php 
			if(get_field('date')):
			$start_date = get_field('date', false, false);
			$start_date = new DateTime($start_date);
			$end_date = get_field('end_date', false, false);
			$end_date = new DateTime($end_date);
			?>
				<span class='event-time'>
					<?php echo $start_date->format('F j'); ?>
					<?php if(get_field('end_date')): ?>
						- <?php echo $end_date->format('F j'); ?>
					<?php endif; ?>
				</span>
			<?php endif; ?>

			<?php if(get_field('venue')): ?>
				<div class='event-location'>
					<span><?php the_field('venue'); ?></span>
					<span><?php the_field('city'); ?>, <?php the_field('state'); ?></span>
				</div>
			<?php endif; ?>

		</header>
	</div>

</div>

