<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

    <div class="row">

        <div class="small-12 columns no-padding-right">

            <div class="input-group show-for-medium fgt-top-search">
                <input type="search" class="input-group-field search-field" placeholder="<?php echo esc_attr_x( 'Search for festivals, artists, concerts', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointstheme' ) ?>" />

                <div class="input-group-button">
                    <a class="button search-submit fgt-search-button" href="#"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/icn-search.png' ?>" /></a>
                    <!--<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Search', 'jointstheme' ) ?>" />-->
                </div>
            </div>

        </div>

    </div>

</form>