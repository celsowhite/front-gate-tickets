<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-position="right">

	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

		<div class="row">

			<div class="small-12 columns no-padding-right">

				<input type="hidden" name="orderby" value="date" />

				<div class="input-group fgt-top-search" style="width: 95%;">
					<input type="search" class="input-group-field search-field" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointstheme' ) ?>" />

					<div class="input-group-button">
						<a class="button search-submit fgt-search-button" href="#"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/icn-search.png' ?>" /></a>
						<!--<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Search', 'jointstheme' ) ?>" />-->
					</div>
				</div>

			</div>

		</div>

	</form>

	<?php
	$backup = $wp_query;
	$wp_query = NULL;
	$wp_query = new WP_Query(array('post_type' => 'post'));

	joints_off_canvas_nav();

	$wp_query = $backup;

	?>
</div>